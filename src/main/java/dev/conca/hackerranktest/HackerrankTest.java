package dev.conca.hackerranktest;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

public class HackerrankTest {

    private Class<? extends Object> classUnderTest;
    
    private String consoleInputResource;
    private String expectedConsoleOutputResource;

    private String consoleOutput;

    public void executeAndAssert() {
        try (InputStream in = HackerrankTest.class.getClassLoader().getResourceAsStream(consoleInputResource)) {
            System.setIn(in);
        
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(baos);
            System.setOut(ps);
            
            Method mainMethod = classUnderTest.getMethod("main", String[].class);
            
            String[] args = {};
            mainMethod.invoke(null, new Object[]{args});
            
            System.out.flush();
            consoleOutput = baos.toString();
            
            assertResults();
            
        } catch (IOException | NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }


    private void assertResults() {
        try {
            URL expectedConsoleOutputResourceUrl = Resources.getResource(expectedConsoleOutputResource);
            String expectedConsoleOutput;
            expectedConsoleOutput = Resources.toString(expectedConsoleOutputResourceUrl, Charsets.UTF_8);

            assertEquals(
                    normalizeEndOfLine(expectedConsoleOutput.trim()),
                    normalizeEndOfLine(consoleOutput.trim()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        } 
    }

    private String normalizeEndOfLine(String string) {
        return string.replaceAll("\r\n+", "\n");
    }


    public void setClassUnderTest(Class<? extends Object> classUnderTest) {
        this.classUnderTest = classUnderTest; 
    }

    public void setConsoleInputResource(String consoleInputResource) {
        this.consoleInputResource = consoleInputResource;
    }
    
    public void setExpectedConsoleOutputResource(String expectedConsoleOutputResource) {
        this.expectedConsoleOutputResource = expectedConsoleOutputResource;
    }

}
