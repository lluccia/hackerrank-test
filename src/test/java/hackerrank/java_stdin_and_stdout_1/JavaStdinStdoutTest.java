package hackerrank.java_stdin_and_stdout_1;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import dev.conca.hackerranktest.HackerrankTest;
import hackerrank.java_stdin_and_stdout_1.Solution;

// https://www.hackerrank.com/challenges/java-stdin-and-stdout-1

@RunWith(Parameterized.class)
public class JavaStdinStdoutTest {
    
    private static final String RESOURCE_PATH = "hackerrank/java_stdin_and_stdout_1/";
    private static final int TESTCASES = 1;
    
    @Parameters(name="testcase_{0}")
    public static Iterable<? extends Object> data() {
        return IntStream.range(0, TESTCASES)
                  .mapToObj( i -> String.format("%02d", i))
                  .collect(Collectors.toList());
    }
    
    @Parameter
    public String testCase;
    
    @Test
    public void test() {
        HackerrankTest ht = new HackerrankTest();

        ht.setConsoleInputResource(RESOURCE_PATH + "input/input" + testCase + ".txt");
        ht.setExpectedConsoleOutputResource(RESOURCE_PATH + "output/output" + testCase + ".txt");
        
        ht.setClassUnderTest(Solution.class);
        
        ht.executeAndAssert();
    }

}
